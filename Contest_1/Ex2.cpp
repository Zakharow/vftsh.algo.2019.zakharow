#include<iostream>
#include<deque>

void ConvertMass(std::deque<int>& deque) {
	while (deque.size() != 1) {
		int sum = deque[0] + deque[1];
		if (sum % 2 != 0) {
			sum = deque[0] - deque[1];
		}
		deque.pop_front();
		deque.pop_front();
		deque.push_back(sum);
	}
	std::cout << deque[0] << std::endl;
}

int main() {
	std::deque<int> deque;
	int n;
	int element;
	std::cin >> n;
	for (int i = 0; i < n; ++i) {
		std::cin >> element;
		deque.push_back(element);
	}
	ConvertMass(deque);
}
