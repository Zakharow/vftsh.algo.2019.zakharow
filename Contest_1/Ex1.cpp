#include <iostream>
#include <string>
#include <vector>

void AddCloseBreacket(char Breacket, int &value, std::vector<char>& Arr) {
	if (Arr[Arr.size() - 1] == Breacket) {
		Arr.pop_back();
	    --value;
	}
	else {
		value = -1;

	}
}

void CheckStack(std::string sequence) {
	int value = 0;
	std::vector<char> Arr;

	if ((sequence[0] == ']') || (sequence[0] == '}') || (sequence[0] == ')')) {
		value = -1;
	}
	else {
		for (int i = 0; i < sequence.length(); i++) {

			if ((sequence[i] == '[') || (sequence[i] == '{') || (sequence[i] == '(')) {
				Arr.push_back(sequence[i]);
				++value;
				continue;
			}

			if (sequence[i] == ']') {
				AddCloseBreacket('[', value, Arr);
				if (value == -1) {
					break;
				}
			}

			else if (sequence[i] == '}') {
				AddCloseBreacket('{', value, Arr);
				if (value == -1) {
					break;
				}
			}

			else if (sequence[i] == ')') {
				AddCloseBreacket('(', value, Arr);
				if (value == -1) {
					break;
				}
			}
		}
	}
	if (value != 0) {
		std::cout << "Incorrect" << std::endl;
	}
	else {
		std::cout << "Correct" << std::endl;
	}
}

int main() {
	std::string sequence;
	std::cin >> sequence;
	CheckStack(sequence);
}
