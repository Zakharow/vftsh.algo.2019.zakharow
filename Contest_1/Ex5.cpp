#include <iostream>

int main() {
	int n;
	std::cin >> n;
	if ((n != 1) && (n != 2)) {
		int element = n;
		for (int i = 0; i < n; ++i) {
			std::cout << element << " ";
			--element;
		}
	}
	else {
		std::cout << -1 << std::endl;
	}
}
