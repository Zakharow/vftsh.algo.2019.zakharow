#include <iostream>
#include <vector>
#include <algorithm>

int main() {
	std::vector<int> arr;
	int quantity;
	std::cin >> quantity;
	int evenNumberCounter = 0;
	int oddNumberCounter = 0;

	for (int i = 0; i < quantity; ++i) {
		int element;
		std::cin >> element;
		if (element % 2 == 0) {
			++evenNumberCounter;
		}
		else {
			++oddNumberCounter;
		}
		arr.push_back(element);
	}

	if (!((quantity == evenNumberCounter) || (quantity == oddNumberCounter))) {
		std::sort(arr.begin(), arr.end());
	}

	for (int i = 0; i < quantity; ++i) {
		std::cout << arr[i] << " ";
	}
}
