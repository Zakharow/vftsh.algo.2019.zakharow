#include <iostream>
#include <queue>
#include <unordered_map>
#include <unordered_set>

using std::unordered_map;
using std::unordered_set;
using std::queue;

int Bfs(unordered_map<int, unordered_set<int>>& graph, int last) {
	unordered_map<int, int> prev;
	int count = 0;
	queue<int> q;
	q.push(1);
	while (!q.empty()) {
		int t = q.front();
		q.pop();
		for (auto& i : graph[t]) {
			q.push(i);
			prev[i] = t;
			if (i == last) {
				while (last != 1) {
					last = prev[last];
					++count;
				}
				return count;
			}
		}
	}
	return -1;
}

int main() {
	unordered_map<int, unordered_set<int>> railway;
	unordered_map<int, unordered_set<int>> roadway;
	int m;
	int n;
	std::cin >> n;
	std::cin >> m;
	if (m == n * (n - 1) / 2) {
		std::cout << -1 << std::endl;
	}
	else {
		for (int i = 0; i < m; ++i) {
			int first;
			int second;
			std::cin >> first;
			std::cin >> second;
			railway[first].insert(second);
			railway[second].insert(first);
		}

		if (railway[1].find(n) != railway[1].end()) {
			for (int i = 1; i < n + 1; ++i) {
				for (int j = 1; j < n + 1; ++j) {
					if ((railway[i].find(j) == railway[i].end()) && (i != j)) {
						roadway[i].insert(j);
					}
				}
			}
			std::cout << Bfs(roadway, n) << std::endl;
		}
		else {
			std::cout << Bfs(railway, n) << std::endl;
		}
	}
}
