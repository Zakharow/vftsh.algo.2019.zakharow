#include <iostream>
#include <unordered_map>
#include <string>
#include <unordered_set>
#include <queue>
#include <vector>

using std::unordered_map;
using std::unordered_set;
using std::string;
using std::queue;
using std::vector;

struct coctail{
	unordered_set<string> Sostav;
	unordered_set<string> Edges;
};

int main() {
	unordered_map<string, coctail> menu;
	unordered_map<string, int> dist;
	unordered_map<string, string> prev;
	queue<string> q;
	vector<string> sequenceDrinks;

	int n;
	std::cin >> n;

	for (int i = 0; i < n; ++i){
		coctail c;
		string name;
		std::cin >> name;
		dist[name] = 1;
		int numberIngr;
		std::cin >> numberIngr;
		for (int g = 0; g < numberIngr; ++g){
			string Ingr;
			std::cin >> Ingr;
			c.Sostav.insert(Ingr);
			for (auto& j : menu) {
				for (auto& h : j.second.Sostav) {
					if (Ingr == h) {
						j.second.Edges.insert(name);
						c.Edges.insert(j.first);
					}
				}
			}
		}
		menu[name] = c;
	}

	string S;
	string F;
	std::cin >> S;
	std::cin >> F;
	dist[S] = 0;
	q.push(S);
	while (!q.empty()) {
		string t = q.front();
		q.pop();
		for (auto& i : menu[t].Edges) {
			if (dist[i]) {
				dist[i] = 0;
				q.push(i);
				prev[i] = t;
			}
		}
	}
	std::cout << dist[F] << std::endl;
	if (dist[F]) {
		std::cout << S << std::endl;
		std::cout << F << std::endl;
	}
	else {
		while (F != S) {
			sequenceDrinks.push_back(F);
			F = prev[F];
		}
		sequenceDrinks.push_back(S);
		for (int i = 1; i < sequenceDrinks.size() + 1; ++i) {
			std::cout << sequenceDrinks[sequenceDrinks.size() - i] << std::endl;
		}
	}
}