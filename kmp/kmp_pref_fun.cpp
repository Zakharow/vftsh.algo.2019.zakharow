#include <iostream>
#include <string>
#include <vector>

using std::vector;
using std::string;
using std::cout;
using std::endl;
using std::cin;

void Pref_function(string a, vector<int>& pi) {
    int j = 0;
    int i = 1;
    int size = a.size();
    while (i < size) {
        if (a[i] == a[j]) {
            pi[i] = j + 1;
            ++j;
            ++i;
        }
        else {
            if (j == 0) {
                pi[i] = 0;
                ++i;
            }
            else {
                j = pi[j - 1];
            }
        }
    }
}

int KMP(string b, string a, vector<int>& pi) {
    int k = 0; // iterator for b
    int l = 0; // iterator for a
    while (k != b.size()) {
        if (b[k] == a[l]) {
            ++k;
            ++l;
            if (l == a.size()) {
                return k - a.size() + 1;
            }
        }
        else {
            if (l == 0){
                ++k;
            }
            else{
                l = pi[l-1];
            }
        }
    }
    return 0;
}

int main() {
    string a,b;
    cin >> b;
    cin >> a;
    vector<int> pi_a(a.size(), 0);
    Pref_function(a, pi_a); // creat pi for a
    for(auto &i : pi_a) {
        cout << i << " ";
    }
    cout << "" << endl;
    cout << KMP(b,a,pi_a) << endl;
}
