#include <iostream>
#include <ctime>

class QSort{
public:
	void QuickSort(int* arr, int first, int last) {

		int i = first;
		int g = last;
		int median = *(arr + (g + i) / 2);

		while (i <= g) {
			while (*(arr + i) < median) {
				i++;
			}
			while (*(arr + g) > median) {
				g--;
			}
			if (i <= g) {
				int value = *(arr + i);
				*(arr + i) = *(arr + g);
				*(arr + g) = value;
				i++;
				g--;
			}
		}

		if (g > first) {
			QuickSort(arr, first, g);
		}
		if (i < last) {
			QuickSort(arr, i, last);
		}
	}
};

void PrintMas(int* arr, int SIZE) {
	for (int j = 0; j < SIZE; ++j) {
		std::cout << *(arr + j) << std::endl;
	}
}


int main() {
	QSort ObjSort;
	srand(time(NULL));

	const int SIZE = 6;
	int arr[SIZE]{};
	int *pArr = arr;

	for (int i = 0; i < SIZE; ++i){
		*(pArr + i) = rand() % 20;
	}
	
	ObjSort.QuickSort(arr, 0, SIZE - 1);
	PrintMas(arr, SIZE);

}
