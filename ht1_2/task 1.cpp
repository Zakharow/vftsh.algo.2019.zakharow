#include<iostream>
#include<list>
#include<ctime>

using std::list;

int main() {
	srand(time(NULL));
	list<int> mylist;
	const int size = 15;
	int k = 5;
	for (int i = 0; i < size; ++i) {
		mylist.push_back(rand() % 20);//creat list
	}
	std::cout << "list was created" << std::endl;
	for (auto& it : mylist) {
		std::cout << it << " ";
	}

	auto it = mylist.begin();
	advance(it, k);
	mylist.insert(mylist.end(), mylist.begin(), it);
	mylist.erase(mylist.begin(), it);

	std::cout << "" << std::endl;
	std::cout << "replacement done" << std::endl;

	for (auto& it : mylist) {
		std::cout << it << " ";
	}
}