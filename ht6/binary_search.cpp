#include <iostream>
#include <vector>
#include <ctime>
using std::vector;

void BinPush(int element, vector<int>& arr) {
	int left = 0;
	int right = arr.size() - 1;

	while (right >= left) {
		int middle = (left + right) / 2;
		if (element < arr[middle]) {
			right = middle - 1;
		}
		else {
			left = middle + 1;
		}
	}
	arr.insert(arr.begin() + left, element);
}

int main() {
	vector<int> Arr;
	srand(time(NULL));
	for (int i = 0; i < 15; ++i) {
		BinPush(rand() % 20, Arr);
	}
	for (int i = 0; i < 15; ++i) {
		std::cout << Arr[i] << " ";
	}
}