#include <iostream>
#include <vector>
#include <ctime>
using std::vector;

void CountSort(vector<int>& arr) {
	vector<int> countArr;
	bool flag = false;
	for (int i = 0; i < arr.size(); ++i) {
		for (int j = 0; j < countArr.size(); ++j) {
			if (arr[i] == countArr[j]) {
				++countArr[j + 1];
				flag = true;
				break;
			}
			++j;
		}
		if (!flag) {
			countArr.push_back(arr[i]);
			countArr.push_back(1);
		}
		flag = false;
	}

	for (int i = 0; i < 10; ++i) {
		std::cout << arr[i] << " ";
	}
	std::cout << "" << std::endl;

	arr.clear();
	for (int j = 0; j < countArr.size(); ++j) {
		for (int k = 0; k < countArr[j + 1]; ++k) {
			arr.push_back(countArr[j]);
		}
		++j;
	}
}

int main() {
	vector<int> Arr;
	srand(time(NULL));

	for (int i = 0; i < 10; ++i) {
		Arr.push_back(rand() % 5);
	}

	CountSort(Arr);

	for (int i = 0; i < 10; ++i) {
		std::cout << Arr[i] << " ";
	}
}