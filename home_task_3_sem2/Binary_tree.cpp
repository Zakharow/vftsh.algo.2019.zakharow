#include <iostream>
#include <unordered_set>
#include <ctime>
#include <vector>
#include <string>

using std::pair;
using std::cout;
using std::endl;
using std::unordered_set;
using std::vector;
using std::string;

struct Vertex {
    int key;
    string value;
    Vertex* left;
    Vertex* right;
};

class BinaryTree {
public:
    void Sort_tree() {
        Sort(Support);
    }

    void Add_to_tree(int element){
        Push(element, Support);
    }

    bool Find_in_tree(int element){
        return Find(Support,element);
    }

    void Delete_from_tree(int element){
        DeleteNode(Support, element);
    }

    ~BinaryTree() {
        DeleteTree(Support);
    }
private:
    Vertex* Support = NULL;

    void Push(int element,Vertex*& TreeV) {
        if (TreeV == NULL) {
            TreeV = new Vertex;
            TreeV->key = element;
            TreeV->value = "0";
            TreeV->left = NULL;
            TreeV->right = NULL;
        }
        else {
            if (element > TreeV->key) {
                Push(element, TreeV->right);
            }
            else {
                Push(element, TreeV->left);
            }
        }
    }

    bool Find(Vertex* tree, int element) {
        if(tree == NULL) {
            return 0;
        }
        if (element == tree->key) {
            return 1;
        }
        if(element > tree->key) {
            return Find(tree->right, element);
        }
        else {
            return Find(tree->left, element);
        }
    }

    void Sort(Vertex* Support) {
        if (Support != NULL) {
            Sort(Support->left);
            cout << Support->key << " ";
            Sort(Support->right);
        }
    }

    Vertex* DeleteNode(Vertex*& tree, int element) {
        if (tree == NULL) {
            return tree;
        }
        if (element == tree->key) {
            Vertex* tmp1;
            if (tree->right == NULL) {
                tmp1 = tree->left;
            }
            else {
                Vertex *tmp2 = tree->right;
                if (tmp2->left == NULL) {
                    tmp2->left = tree->left;
                    tmp1 = tmp2;
                }
                else {
                    Vertex* minVertex = tmp2->left;
                    while (minVertex->left != NULL) {
                        tmp2  = minVertex;
                        minVertex = tmp2->left;
                    }
                    tmp2->left   = minVertex->right;
                    minVertex->left  = tree->left;
                    minVertex->right = tree->right;
                    tmp1 = minVertex;
                }
            }
            delete tree;
            return tmp1;
        }
        else if (element < tree->key) {
            tree->left = DeleteNode(tree->left, element);
        }
        else {
            tree->right = DeleteNode(tree->right, element);
        }
        return tree;
    }

    void DeleteTree(Vertex*& Tree) {
        if (Tree != NULL) {
            DeleteTree(Tree->left);
            DeleteTree(Tree->right);
            delete Tree;
            Tree = NULL;
        }
    }
};

int main() {
    srand(time(NULL));
    unordered_set<int> Ver;
    BinaryTree MyOwnTree;
    int testVertex;
    for (int i = 0; i < 25; ++i) {
        testVertex = rand() % 100;
        Ver.insert(testVertex);
    }

    for (auto &i : Ver) {
        MyOwnTree.Add_to_tree(i);
    }

    MyOwnTree.Sort_tree();

    cout<<""<<endl;

    if (MyOwnTree.Find_in_tree(testVertex)) {
        cout << testVertex << " is in the Tree!" << endl;
        MyOwnTree.Delete_from_tree(testVertex);
        cout << testVertex << " was removed!" << endl;
    }
    else {
        cout << testVertex << " is not in the Tree!" << endl;
    }

    if (MyOwnTree.Find_in_tree(testVertex)) {
        cout << testVertex << " is in the Tree!" << endl;
        MyOwnTree.Delete_from_tree(testVertex);
        cout << testVertex << " was removed!" << endl;
    }
    else {
        cout << testVertex << " is not in the Tree!" << endl;
    }

    MyOwnTree.Sort_tree();
}
