#include <iostream>

struct TStackEl {

	int Data;
	TStackEl* Next;
};

class TStack {
	unsigned int Len;
	TStackEl* Head;

public:
	TStack() : Len(0) {}

	void Pop() {
		if (Empty()) {
			std::cout << "Empty" << std::endl;
		}
		else {
			TStackEl* newEl = Head;
			Head = Head->Next;
			delete newEl;
			Len--;
		}
	}

	int Top() {
		if (Empty()) {
			std::cout << "Empty" << std::endl;
		}
		else
		{
			return Head->Data;
		}
	}

	void Push(int x) {
		TStackEl* newEl = new TStackEl;
		newEl->Data = x;
		newEl->Next = Head;
		Head = newEl;
		Len++;
	}

	bool Empty() {
		return Len == 0;
	}

	unsigned int Size() {
		return Len;
	}
};

int main() {
	TStack ObjStack;
	ObjStack.Push(10);
	std::cout << ObjStack.Top() << std::endl;
	
	ObjStack.Push(11);
	ObjStack.Push(13);
	std::cout <<"Size: " << ObjStack.Size() << std::endl;

	if (ObjStack.Empty()) {
		std::cout << "Stack is empty." << std::endl;
	}
	else {
		std::cout << "Stack is not empty." << std::endl;
	}
}
