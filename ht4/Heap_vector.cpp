#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>

class Heap {
	std::vector<int> Arr;
public:
	void MakeHeap(std::vector<int>& Arr1) {
		if (Arr.size() == 0) {
			Arr.push_back(0);
		}
		for (int i = 0; i < Arr1.size(); ++i) {
			Arr.push_back(Arr1[i]);
			ShiftUp(Arr.size() - 1);
		}
	}

	int Maximum() {
		if (Arr.size() - 1 == 1) {
			int max = Arr[1];
			Arr.pop_back();
			return max;
		}
		if (Arr.size() - 1 == 2) {
			if (Arr[1] > Arr[2]) {
				swap(&Arr[1], &Arr[2]);
			}
			int max = Arr[2];
			Arr.pop_back();
			return max;
		}
		int max = Arr[1];
		swap(&Arr[1], &Arr[Arr.size() - 1]);
		Arr.pop_back();
		ShiftDown(1);
		return max;
	}
private:
	void ShiftDown(int index) {
		int maxDescendant;
		if (2 * index + 1 < Arr.size() && Arr[2*index + 1] > Arr[2 * index]) {
			maxDescendant = 2 * index + 1;
		}
		else {
			maxDescendant = 2 * index;
		}
		if (maxDescendant < Arr.size() && Arr[maxDescendant] > Arr[index]) {
			swap(&Arr[index], &Arr[maxDescendant]);
			ShiftDown(maxDescendant);
		}
	}

	void ShiftUp(int index) {
		while (Arr[index] > Arr[index / 2]) {
			if (index == 1) {
				break;
			}
			swap(&Arr[index], &Arr[index / 2]);
			index = index / 2;
		}
	}

	void swap(int* a, int* b) {
		int value = *a;
		*a = *b;
		*b = value;
	}
};

int getRandomNumber(int min, int max) {
	static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
	return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

void HeapSort(std::vector<int>& Arr) {
	Heap Object;
	std::vector<int> SortArr;
	Object.MakeHeap(Arr);
	for (int i = 0; i < Arr.size(); ++i) {
		SortArr.push_back(Object.Maximum());
	}
	Arr = SortArr;
}

int main() {
	std::vector<int> Arr;

	srand(static_cast<unsigned int>(time(0)));
	const int number = 15;

	for (int j = 0; j < number; ++j) {
		Arr.push_back(getRandomNumber(0, 1000));
	}
	std::cout << "Massive was created!" << std::endl;
	for (int i = 0; i < Arr.size(); ++i) {
		std::cout << Arr[i] << " ";
	}
	std::cout << "" << std::endl;

	HeapSort(Arr);

	std::cout << "Massive was sorted!" << std::endl;
	for (int i = 0; i < Arr.size(); ++i) {
		std::cout << Arr[i] << " ";
	}
}
